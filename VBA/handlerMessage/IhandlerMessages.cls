VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "IhandlerMessages"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Compare Database
Option Explicit

'Return error messages with one message per row
Public Property Get ErrorMessages() As String: End Property

'Return all messages with one message per row
Public Property Get AllMessages() As String: End Property

'Return hint messages with one message per row
Public Property Get HintMessages() As String: End Property

'Show error message instantly? (Default: False)
Public Property Let setErrorMessagesPopUp(p_Show As Boolean): End Property

'Show hint message instantly? (Default: False)
Public Property Let setHintMessagesPopUp(p_Show As Boolean): End Property

'Add error message
Public Property Let addErrorMessage(p_Message As String): End Property

'Add hint message
Public Property Let addHintMessage(p_Message As String): End Property

'Delete all messages
Public Sub resetAllMessages(): End Sub


'***** FILES
'*
'Static Functions - Modul handlerMessages_Static
'Objects - Class Modul handlerMessages
'Interfaces - Class Modul IhandlerMessages
'*
'***** END Files
