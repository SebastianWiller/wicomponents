VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "handlerMessages"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Compare Database
Option Explicit

'***** Interfaces
'*
Implements IhandlerMessages
'*
'***** END Interfaces

'***** Backend Konstanten
'*

'*
'***** END Backend Konstanten

'***** Enums
'*
Private Enum MessageTyp
    T_ERROR
    T_HINT
    T_ALL '= Hint & Error
End Enum
'*
'***** END Enums

'***** Variablen
'*
Private c_Messages() As String
Private c_Typs() As String
Private c_PopUp_Error As Boolean
Private c_PopUp_Hint As Boolean
'*
'***** END Variablen

'***** Klasse
'*
Private Sub Class_Initialize()
    c_PopUp_Error = False
    c_PopUp_Hint = False
    ReDim c_Messages(0)
    ReDim c_Typs(0)
End Sub

Private Sub Class_Terminate()

End Sub

Private Function Message(ByVal p_Message As String, ByVal p_Typ As MessageTyp) As String
    Message = handlerMessages_Static.Message
End Function

Private Sub showMessage(p_Message As String, p_Typ As MessageTyp)
    Debug.Print Message(p_Message, p_Typ)
    MsgBox Message(p_Message, p_Typ)
End Sub

Private Function getMessageTitel(p_Typ As MessageTyp) As String
    Dim returnValue As String
    
    returnValue = ""
    
    Select Case p_Typ
        Case MessageTyp.T_ERROR:
            returnValue = "Error"
        Case MessageTyp.T_HINT:
            returnValue = "Hinweis"
        Case MessageTyp.T_ALL:
            returnValue = "Nachricht"
    End Select
    getMessageTitel = returnValue
End Function

Private Function getMessages(Optional p_Typ As MessageTyp) As String
    Dim returnValue As String
    Dim i As Integer
    
    returnValue = ""
    
    Select Case p_Typ
        Case MessageTyp.T_ERROR:
            For i = 0 To UBound(c_Messages) - 1
                If c_Typs(i) = MessageTyp.T_ERROR Then
                    returnValue = returnValue & vbCrLf & _
                        Message(c_Messages(i), c_Typs(i))
                End If
            Next i
        Case MessageTyp.T_HINT:
            For i = 0 To UBound(c_Messages) - 1
                If c_Typs(i) = MessageTyp.T_HINT Then
                    returnValue = returnValue & vbCrLf & _
                        Message(c_Messages(i), c_Typs(i))
                End If
            Next i
        Case MessageTyp.T_ALL:
            For i = 0 To UBound(c_Messages) - 1
                returnValue = returnValue & vbCrLf & _
                    Message(c_Messages(i), c_Typs(i))
            Next i
        Case Else:
            'Default
    End Select
    
    getMessages = returnValue
End Function

Private Sub addMessage(p_Typ As MessageTyp, p_Message As String)
    'Save
    ReDim Preserve c_Messages(UBound(c_Messages) + 1)
    c_Messages(UBound(c_Messages) - 1) = p_Message
    
    ReDim Preserve c_Typs(UBound(c_Typs) + 1)
    c_Typs(UBound(c_Typs) - 1) = p_Typ
    
    'PopUp
    Select Case p_Typ
        Case MessageTyp.T_ERROR:
            If c_PopUp_Error Then
                showMessage p_Message, p_Typ
            End If
    
        Case MessageTyp.T_HINT:
            If c_PopUp_Hint Then
                showMessage p_Message, p_Typ
        End If
    End Select
End Sub
'*
'***** END Klasse

'***** Interface IhandlerMessages
'*
'*
Public Property Get IhandlerMessages_ErrorMessages() As String
    IhandlerMessages_ErrorMessages = getMessages(T_ERROR)
End Property

Public Property Get IhandlerMessages_AllMessages() As String
    IhandlerMessages_AllMessages = getMessages(T_ALL)
End Property

Public Property Get IhandlerMessages_HintMessages() As String
    IhandlerMessages_HintMessages = getMessages(T_HINT)
End Property

Public Property Let IhandlerMessages_setErrorMessagesPopUp(p_Show As Boolean)
    c_PopUp_Error = p_Show
End Property

Public Property Let IhandlerMessages_setHintMessagesPopUp(p_Show As Boolean)
    c_PopUp_Hint = p_Show
End Property

Public Property Let IhandlerMessages_addErrorMessage(p_Message As String)
    addMessage T_ERROR, p_Message
End Property

Public Property Let IhandlerMessages_addHintMessage(p_Message As String)
    addMessage T_HINT, p_Message
End Property

Public Sub IhandlerMessages_resetAllMessages()
    ReDim c_Messages(0)
    ReDim c_types(0)
End Sub
'*
'*
'***** END Interface IhandlerMessages
